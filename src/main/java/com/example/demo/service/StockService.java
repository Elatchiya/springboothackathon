package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//import com.example.demo.entities.Shipper;
import com.example.demo.repository.StockRepository;
import java.util.List;

@Service

public class StockService {
	@Autowired
	private StockRepository repository;
	public List<Stocks> getAllStocks(){
		return repository.getAllStocks();
	}
	
	public Stocks getShipper(int id) {
		return repository.getStocksById(id);
	}

	public Shipper saveShipper(Shipper shipper) {
		return repository.editShipper(shipper);
	}

	public Shipper newShipper(Shipper shipper) {
		return repository.addShipper(shipper);
	}

	public int deleteShipper(int id) {
		return repository.deleteShipper(id);
	}
}
