package com.example.demo.controller;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Stocks;
import com.example.demo.repository.StockRepository;

@RestController
public class StockController {
	@Autowired
	private StockRepository stockRepository;
	@GetMapping("/stocks")
	public Stocks stocks(@RequestParam(required = false, defaultValue = "HDFC") String name) {
		String str = "BUY/SELL " + name + "!";
		if (name.equals("HDFC")) {
			name = "Default: HDFC";
		}
		Stocks newStock = new Stocks(name);

		// Save the object (H2 DB)
		Stocks savedObject = stockRepository.save(newStock);
		// Return the object from H2 to a client

		return savedObject;

	}
}
