package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Stocks;
public interface StockRepository extends JpaRepository<Stocks,Long>{
	public List<Stocks> getAllStocks();
	public Stocks getStocksById(int id); 
	public Stocks editStocks(Stocks shipper);
	public int deleteStocks(int id);
	public Stocks addStocks(Stocks shipper);
}
